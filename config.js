var fs = require("fs");

module.exports = {
    mongoDB: {
        host: "127.0.0.1",
        port: "27017",
        user: "mail-suite-user",
        password: "Iwerotihi432",
        database: "mail-suite"
    },
    
    smtpModules: [{
        id: "ServerInsecure",
        host: "0.0.0.0",
        port: 25,
        
        allowOutgoing: true,
        allowExternIncoming: true,
        allowInternIncoming: true,
        
        secure: false,
        key: fs.readFileSync("cert/key.pem"),
        cert: fs.readFileSync("cert/cert.pem"),
    
        authOptional: true,
        
    }]
};