var fs = require("fs");
var SmtpServer = require("smtp-server").SMTPServer;
var MailSuite = require("./lib/MailSuite");

var config = require("./config");
var log = require("./lib/Logging");

var mailSuite = new MailSuite(config, log);
mailSuite.initialize();


var SMTPConfigFields = ["name", "banner", "size", "authMethods", "authOptional", "disabledCommands", "hideSTARTTLS", "hidePIPELINING",
                        "hide8BITMIME", "hideSMTPUTF8", "allowInsecureAuth", "sniOptions", "maxClients", "useProxy", "useXClient",
                        "useXForward", "lmtp", "socketTimeout", "closeTimeout", "key", "cert"];

/*var plainServer = new SmtpServer({
    secure: false,
    key: fs.readFileSync("cert/key.pem"),
    cert: fs.readFileSync("cert/cert.pem"),
    
    authOptional: true,
    
    onConnect: function(session, callback){
        console.log("Connected");
        if(session.remoteAddress === '127.0.0.1'){
            return callback(new Error('No connections from localhost allowed'));
        }
        return callback(); // Accept the connection 
    },
    onData: function(stream, session, callback){
        console.log("Incoming data");
        stream.pipe(process.stdout); // print message to console 
        stream.on('end', callback);
    }
});


plainServer.on('error', function(err){
    console.log('Error %s', err.message);
});
plainServer.listen(25);*/

console.log("Server listening");