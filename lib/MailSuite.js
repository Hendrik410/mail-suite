var SmtpServer = require("smtp-server").SMTPServer;

var Utils = require("./lib/Utils");
var DatabaseManager = require("./lib/DatabaseManager");
var MailQueue = require("./lib/MailQueue");

var SMTPConfigFields = ["name", "banner", "size", "authMethods", "authOptional", "disabledCommands", "hideSTARTTLS", "hidePIPELINING",
                        "hide8BITMIME", "hideSMTPUTF8", "allowInsecureAuth", "sniOptions", "maxClients", "useProxy", "useXClient",
                        "useXForward", "lmtp", "socketTimeout", "closeTimeout", "key", "cert"];

module.exports = function(config, log){
    this.config = config;
    this.log = log;
    
    this.smtpModules =  [];
    this.incomingQueue = undefined;
    this.outgoingQueue = undefined;
    
    this.initialize = function(){
        // initialize database connection
        this.db = new DatabaseManager(this.config.mongoDB, this.log);
        if(!this.db.connect()){
            log.fatal("Error while connecting to database");
            process.exit(-1);
        }
        
        this.incomingQueue = new MailQueue(this.db.getCollection("IncomingQueue"));
        this.outgoingQueue = new MailQueue(this.db.getCollection("OutgoingQueue"));
        
        for(var i = 0; i < config.smtpModules.length; i++){
            var moduleConfig = config.smtpModules[i];
            
            var module = {
                name: moduleConfig.id,
                host: moduleConfig.host,
                port: moduleConfig.port
            };
            module.server = new SmtpServer(Utils.filterAttributes(moduleConfig, SMTPConfigFields));
            module.s
        }
    };
};