var deasync = require("deasync");
var mongoDb = require("mongoDb");

module.exports = {
    
    /**
     * Creates a copy of an object, only containing the specified attributes.
     * 
     * @param object The object to copy.
     * @param attributes The attributes to filter for.
     * @returns The cpoied object.
     */
    filterAttributes: function(object, attributes){
        var out = {};
        for(var entry of attributes){
            if(object[entry])
                out[entry] = object[entry];
        }
        return out;
    },
    
    /**
     * Creates a copy of an object, only containing the specified attributes. Non-existant attributes will be created.
     * 
     * @param object The object to copy.
     * @param attributes The attributes to filter for.
     * @returns The cpoied object.
     */
    normalizeAttributes: function(object, attributes){
        var out = {};
        for(entry of attributes){
            if(object[entry])
                out[entry] = object[entry] ? object[entry] : "";
        }
        return out;
    },
    
    
    /**
     * Counts the objects in a collection matching the given selector.
     * 
     * @param collection The collection to search in.
     * @param selector The selector to search with.
     * @returns The count of the matching entries.
     */
    getCountInCollection: function(collection, selector){
        var finished = false;
        var count = 0;
        collection.count(selector, function(err, c){
            count = c;
            finished = true;
        });
        while(!finished)
            deasync.runLoopOnce();
        
        return count;
    },
    
    
    /**
     * Gets the document with the give id out of the given collection.
     * 
     * @param collection The collection to search in.
     * @param rawId The id to search for.
     * @param acceptedType Returns an error if the field "type" does not equal this parameter (optional).
     * @returns The retrieved object or one of the getItemErrors.
     */
    getItemFromCollection: function(collection, rawId){
        var item = null;
        var finished = false

        if (rawId.length == 24) {
            collection.findOne({ "_id": new mongoDb.ObjectID(rawId) }, function (err, i) {
                item = i;
                finished = true;
            });
            while (!finished)
                deasync.runLoopOnce();
        }

        if (item == null) {
            return false;
        }
        
        return item;
    }
    
}