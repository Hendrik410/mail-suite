var log = require("log4js");

log.loadAppender("file");
log.addAppender(log.appenders.console(), 'logger');
log.addAppender(log.appenders.file('./log.txt'), 'logger');

var logger = log.getLogger('logger');


module.exports = logger;