var deasync = require("deasync");
var Util = require("./Util");

module.exports = function(collection){
    this.collection = collection;
    
    
    this.push = function(mail){
        var insertedId = null;
        collection.insert(mail, {fullResult: true}, function(err, result){
            insertedId = result.insertedIds[0];
        });
        while(!insertedId)
            deasync.runLoopOnce();
        
        mail = Util.getItemFromCollection(collection, insertedId.toHexString());
        return mail;
    },
    
    this.pop = function(){
        var finished = false;
        var result = null;
        
        collection.findOneAndDelete({}, function(err, obj){
            result = obj.value;
            finished = true;
        });
        while(!finished)
            deasync.runLoopOnce();
        
        return result;
    },
    
    this.getCount = function(){
        var finished = false;
        var result = null;
        
        collection.count({}, function(err, count) {
            result = count;
            finished = true;
        });
        while(!finished)
            deasync.runLoopOnce();
        
        return result;
    }
}