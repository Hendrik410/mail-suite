var mongoDB = require("mongodb");
var deasync = require("deasync");

/**
 * Constructor for a new DatabaseManager
 * 
 * @param config The configuration for the application
 */
function DatabaseManager(config, log) {
    this.config = config;
    this.log = log;
    
    this.db = undefined;
    
    /**
     * Starts a connection to the database.
     * 
     * @returns Returns true if successful, otherwise false.
     */
    this.connect = function(){
        if(this.db)
            return;
        
        
        var instance = this;
        var connectionString = "mongodb://" + config.host + 
            ":" + config.port + "/" + config.database;
        
        mongoDB.MongoClient.connect(connectionString, function(err, db){
            if(err){
                 instance.log.error(err);
                 return false;
            }
            
            instance.db = db;
        });
        
        while(this.db == undefined){
            deasync.runLoopOnce();
        }
        
        log.debug("Successfully connected to the database");
        return true;
    }
    
    /**
     * Fetches the collection with the given name from the database.
     * 
     * @returns The collection with the given name.
     */
    this.getCollection = function(name){
        if(this.db == undefined)
            throw Error("Not connected to database");
        
        return this.db.collection(name);
    }
    
    /**
     * Creates an ObjectID from the give raw id.
     * 
     * @param rawId The raw id to convert.
     * @returns The corresponding ObjectID.
     */
    this.id = function(rawId){
        return new mongoDB.ObjectID(rawId);
    }
}

module.exports = DatabaseManager;